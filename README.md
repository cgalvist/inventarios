# Sistema de inventarios

Fork del original creado por [jiestrada](https://gitlab.com/jiestrada/inventarios).

El tutorial del proyecto ha sido creado por `José Estrada` en la siguiente lista de reproducción en [YouTube](https://www.youtube.com/playlist?list=PLxqaMA9Ag3-YVak1chUglGAhx-4T0YPEq).

Este proyecto es una representación de un sistema de inventarios creado con ASP.NET (MVC y API web).

## Estructura del proyecto

La solución de ASP.NET contiene los siguientes proyectos:

* **Repository:** Contiene interfaces para el acceso a datos
* **EFRepository:** Contiene clases para el acceso a datos utilizando Entity Framework
* **Model:** Contiene la configuración para la conexión a la base de datos, para luego poder crear el modelo del proyecto pirncipal en base a las tablas seleccionadas de la base de datos.
  
## Base de datos

Puede crear la base de datos de ejemplo ejecutando los comandos del script `scriptdb.sql` en SQL Server Management Studio o por consola.

### Diagrama Entidad-Relación

El diagrama Entidad-Relación lo puede visualizar en el archivo `DBEntity.edmx` en el proyecto `Model`.

## Instalación

Luego de crear la base de datos, puede abrir el proyecto `Sys.Inventarios.sln` ubicado en la carpeta `Sys.Inventarios` con Visual Studio.

Después de abrir el proyecto, con el botón secundario del ratón de clic al proyecto y seleccione la opción `Restaurar paquetes de NuGet` para actualizar todos los paquetes NuGet del proyecto.

### Conexión a la base de datos

Para actualizar la conexión a la base de datos, abrimos el archivo `DBEntity.edmx` en el proyecto `Model`, en este caso se abrirá un diagrama Entidad-Relación. Luego de abrirlo, oprima con el botón secundario del ratón en un area vacía del diagrama y seleccione la opción `Actualizar modelo desde base de datos ...`, aparecerá una ventana en donde podrá configurar los datos de conexión del equipo local.

En la ventana anterior seleccionamos `Nueva conexión ...`, luego `Microsoft SQL Server` y por último dar clic al botón `Continuar`. Se abrirá una nueva ventana solicitándonos los datos de nuestro servidor local. Luego de ingresarlos damos clic en el botón `Aceptar` y luego damos clic en el botón `siguiente` en la ventana principal.

Luego de los pasos anteriores damos clic al botón `Finalizar`.

## Autenticación

Usuario de prueba:
* **email:** ejemplo@ejemplo.com
* **contraseña:** ejemplo
﻿using Model;
using Repository;
using Sys.Inventarios.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

/// <summary>
/// Verificar si un usuario está autenticado o no
/// </summary>
namespace Sys.Inventarios.Attributes
{
    /// <summary>
    /// Realizar acción en caso de que el usuario se haya autenticado
    /// </summary>
    public class AutenticadoAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (!SessionHelper.ExistUserInSession())
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "Account",
                    action = "Index"
                }));
            }
            else
            {
                IRepository Repositorio = new Model.Repository();
                int idUser = SessionHelper.GetUser();
                var Usuario = Repositorio.FindEntity<Usuarios>(c => c.Id == idUser);
                if (Usuario != null)
                {
                    SessionHelper.ActualizarSession(Usuario);
                }
            }
        }
    }

    /// <summary>
    /// Realizar acciones en caso de que el usuario no se haya autenticado
    /// </summary>
    public class NoLoginAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// Realizar acción en caso de que el usuario no se haya autenticado
        /// En este caso se redirige al usuario a la página principal
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (SessionHelper.ExistUserInSession())
            {
                IRepository Repositorio = new Model.Repository();
                int idUser = SessionHelper.GetUser();
                var Usuario = Repositorio.FindEntity<Usuarios>(c => c.Id == idUser && c.Activo==true);
                if (Usuario != null)
                {
                    SessionHelper.ActualizarSession(Usuario);
                    if (Usuario.Rol_Id == 1)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            controller = "Admin",
                            action = "Index"
                        }));
                    }
                }
                                
            }
        }
    }

}
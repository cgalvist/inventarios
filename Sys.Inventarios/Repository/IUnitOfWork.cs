﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    /// <summary>
    /// Interfaz que permitirá que los cambios del repositorio se vean reflejados en la base de datos
    /// utilizando la interfaz "IRepository"
    /// </summary>
   public interface IUnitOfWork:IRepository, IDisposable
   {
        int Save();
    }
}

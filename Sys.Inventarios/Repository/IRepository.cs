﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using PagedList;

namespace Repository
{
    /// <summary>
    /// Interfaz de repositorios, para acceso de datos
    /// Esta interfaz utiliza IDisposable para que se destruya automaticamente si no se esta usando
    /// </summary>
    public interface IRepository : IDisposable
    {
        /// <summary>
        /// Crear un registro
        /// </summary>
        TEntity Create<TEntity>(TEntity newEntity) where TEntity : class;

        /// <summary>
        /// Actualizar un registro
        /// </summary>
        bool Update<TEntity>(TEntity updateEntity) where TEntity : class;

        /// <summary>
        /// Eliminar un registro
        /// </summary>
        bool Delete<TEntity>(TEntity deleteEntity) where TEntity : class;

        /// <summary>
        /// Obtener un registro
        /// </summary>
        TEntity FindEntity<TEntity>(Expression <Func<TEntity, bool>> criteria) where TEntity : class;

        /// <summary>
        /// Obtener un listado de entidades (filas)
        /// </summary>
        IEnumerable<TEntity> FindEntitySet<TEntity>(Expression<Func<TEntity, bool>> criteria) where TEntity : class;

        // NOTA: Para esto se utiliza el paquete NuGet "PagedList.Mvc" creado por "Troy Goode"

        /// <summary>
        /// Obtener un listado de entidades (filas) de manera paginada        
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="criteria">Condicion de la consulta</param>
        /// <param name="order">Orden de los registros</param>
        /// <param name="page">La pagina que se desea obtener</param>
        /// <param name="pageSize">Cantidad de registros a obtener</param>
        /// <returns></returns>
        IPagedList<TEntity> FindEntitySetPage<TEntity>(Expression<Func<TEntity, bool>> criteria, Expression<Func<TEntity, string>> order, int page, int pageSize) where TEntity : class;
    }
}
